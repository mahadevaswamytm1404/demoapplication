
public class Item {

    private final String form;
    private final String section;
    private final String items;
    private final String reportingGroup;
    private final String scoredORUnscored;
   
	public Item(String form, String section, String items, String reportingGroup, String scoredORUnscored) {
		super();
		this.form = form;
		this.section = section;
		this.items = items;
		this.reportingGroup = reportingGroup;
		this.scoredORUnscored = scoredORUnscored;
	}
	public String getForm() {
		return form;
	}
	public String getSection() {
		return section;
	}
	public String getItems() {
		return items;
	}
	public String getReportingGroup() {
		return reportingGroup;
	}
	public String getScoredORUnscored() {
		return scoredORUnscored;
	}
    
    
    


}
