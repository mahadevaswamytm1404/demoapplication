import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.hdgf.streams.Stream;


import java.io.FileReader;
import java.io.Reader;

public class CSVFileReader {

	public static void main(String[] args) throws IOException {

		/*
		 * BufferedReader reader =
		 * Files.newBufferedReader(Paths.get("CSVFormatFile.csv")); CSVParser csvParser
		 * = new CSVParser(reader, CSVFormat.DEFAULT.withHeader("Form", "Section",
		 * "Item", "ReportingGroup", "ScoredORUnscored")
		 * .withIgnoreHeaderCase().withTrim());
		 * 
		 * System.out.println("csvRecord : " + csvParser); for (CSVRecord csvRecord :
		 * csvParser) {
		 * 
		 * // Accessing Values by Column Index String form = csvRecord.get(0);
		 * 
		 * // Accessing the values by column header name String section =
		 * csvRecord.get(1); // Accessing Values by Column Index String item =
		 * csvRecord.get(2);
		 * 
		 * // Accessing the values by column header name String reportingGroup =
		 * csvRecord.get(3);
		 * 
		 * // Accessing the values by column header name String scoredORUnscored =
		 * csvRecord.get(4);
		 * 
		 * // Printing the record System.out.println("csvRecord : " +
		 * csvRecord.getRecordNumber());
		 * 
		 * System.out.println("form  : " + form); System.out.println("section : " +
		 * section); System.out.println("item : " + item);
		 * System.out.println("reportingGroup : " + reportingGroup);
		 * System.out.println("scoredORUnscored : " + scoredORUnscored);
		 * 
		 * 
		 * System.out.println("\n\n");
		 * 
		 * }
		 */
		/*
		 * //String fileName = "c:\\test\\csv\\country.csv"; try (CSVReader reader = new
		 * CSVReader(new FileReader("CSVFormatFile.csv"))) { List<String[]> r =
		 * reader.readAll(); r.forEach(x -> System.out.println(Arrays.toString(x))); }
		 */

		/*
		 * Scanner sc = new Scanner(new File("CSVFormatFile.csv"));
		 * sc.useDelimiter(","); //sets the delimiter pattern while (sc.hasNext())
		 * //returns a boolean value { System.out.print(sc.next()); //find and
		 * returnsthe next complete token from this scanner } sc.close();
		 */

		/*
		 * String line = ""; String splitBy = ";"; try { //parsing a CSV file into
		 * BufferedReader class constructor BufferedReader br = new BufferedReader(new
		 * FileReader("CSVFormatFile.csv")); while ((line = br.readLine()) != null)
		 * //returns a Boolean value { String[] employee = line.split(splitBy); // use
		 * comma as separator System.out.println("Employee [Form=" + employee[0] +
		 * ", Section=" + employee[1] + ", Item=" + employee[2] + ", ReportingGroup=" +
		 * employee[3] + ", Scored/unscored= " + employee[4] + ""); } } catch
		 * (IOException e) { e.printStackTrace(); }
		 */

		try (Reader reader = Files.newBufferedReader(Paths.get("CSVFormatFile.csv"));
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withHeader("Form", "Section", "Item", "ReportingGroup").withIgnoreHeaderCase()
								.withTrim().withDelimiter(';').withSkipHeaderRecord());) {

			for (CSVRecord csvRecord : csvParser) { // Accessing values by the name assigned to each column
				String form = csvRecord.get("Form");
				String section = csvRecord.get("Section");
				String item = csvRecord.get("Item");
				String reportingGroup = csvRecord.get("ReportingGroup");

				System.out.println("Record No - " + csvRecord.getRecordNumber());
				System.out.println("---------------");
				System.out.println("Name : " + form);
				System.out.println("Email : " + section);
				System.out.println("Phone : " + item);
				System.out.println("Country : " + reportingGroup);
				System.out.println("---------------\n\n");
			}
		}

		/*
		 * //Pattern comma = Pattern.compile(";"); try (Stream<Reader> stream =
		 * Files.newBufferedReader(Paths.get("CSVFormatFile.csv")); CSVParser csvParser
		 * = new CSVParser(stream, CSVFormat.DEFAULT.withHeader("Form", "Section",
		 * "Item", "ReportingGroup").withIgnoreHeaderCase()
		 * .withTrim().withDelimiter(';').withSkipHeaderRecord());) { Map<Integer,
		 * String> numberOfLessonsPassed = stream.skip(1).map(l -> comma.split(l))
		 * .map(s -> new Item(s[0], s[1], s[2], s[3], s[4])).filter(s -> s.getGrade() >=
		 * 5) .collect(Collectors.groupingBy(Item::getForm, Collectors.counting()));
		 * System.out.println(numberOfLessonsPassed); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */

	}

}
