package com.excelsoft.hotelmanagement.serviceImpl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.excelsoft.hotelmanagement.entity.User;
import com.excelsoft.hotelmanagement.repository.UserRepository;
import com.excelsoft.hotelmanagement.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;

	/*
	 * public UserServiceImpl(UserRepository userRepo) { this.userRepo = userRepo; }
	 */
	@Override
	public List<User> getUsers() {
		return this.userRepo.findAll();
	}

	@Override
	public User createUser(User user) {
		return this.userRepo.save(user);
	}

	@Override
	public User getUserById(long id) {
		return this.userRepo.findById(id).orElseThrow(() -> new RuntimeException("User Not Found"));
	}

	@Override
	public boolean authenticateByUserName(String userName) {
		Optional<User> userOpt = this.userRepo.findByUserName(userName);
		return userOpt.isPresent();
	}

	@Override
	public User findBypassword(String password) {
		return this.userRepo.findBypassword(password);
	}

	@Override
	public User checkUserNameAndPasswordExistOrNot(String userName, String password) {

		return this.userRepo.checkUserNameAndPasswordExistOrNot(userName, password);

	}

	@Override
	public Integer updatePassword(String userName, String password) {
		return this.userRepo.changePassword(userName, password);

	}
}
