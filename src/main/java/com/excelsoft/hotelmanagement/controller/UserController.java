package com.excelsoft.hotelmanagement.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.excelsoft.hotelmanagement.common.Constants;
import com.excelsoft.hotelmanagement.dto.UserRequestDTO;
import com.excelsoft.hotelmanagement.dto.UserResponseDTO;
import com.excelsoft.hotelmanagement.entity.User;
import com.excelsoft.hotelmanagement.service.UserService;

import net.jodah.expiringmap.ExpiringMap;

@RestController
@RequestMapping("users/")
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	private Map<String, String> otpmap = ExpiringMap.builder().expiration(10, TimeUnit.MINUTES).build();

	public Map<String, String> getOtpmap() {
		return otpmap;
	}

	public void addOtpmap(String key, String otp) {
		otpmap.put(key, otp);
	}

	@GetMapping("/getAllUsersDetails")
	public List<User> getUsers() {
		return this.userService.getUsers();
	}

	@PostMapping("/createNewUser")
	public ResponseEntity<UserResponseDTO> createUser(@RequestBody UserRequestDTO userRequestDTO) {
		LOGGER.info("Started Hotel-Management createUser :");
		User user = new User();
		Boolean checkUserExistsOrNot = Boolean.FALSE;
		User creadtedUserDetails = new User();
		UserResponseDTO userResponseDTO = new UserResponseDTO();

		if (userRequestDTO.getUserFirstName() == null || userRequestDTO.getUserFirstName().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getUserLastName() == null || userRequestDTO.getUserLastName().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getUserEmail() == null || userRequestDTO.getUserEmail().isEmpty()
				|| userRequestDTO.getUserEmail().length() <= 0
				|| !userRequestDTO.getUserEmail().matches(Constants.EMAIL_REGEX)) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getPhoneNumber() == null || userRequestDTO.getPhoneNumber().isEmpty()
				|| userRequestDTO.getPhoneNumber().length() <= 0
				|| !userRequestDTO.getPhoneNumber().matches(Constants.PHONE_NUMBER_REGEX)) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else {
			try {
				checkUserExistsOrNot = this.userService.authenticateByUserName(userRequestDTO.getUserEmail());
				if(!checkUserExistsOrNot) {
				user.setUserFirstName(userRequestDTO.getUserFirstName());
				user.setUserLastName(userRequestDTO.getUserLastName());
				user.setUserName(userRequestDTO.getUserEmail());
				int otpRandom = new Random().nextInt((9999 - 100) + 1) + 10;
				//pending otp session  handling
				addOtpmap(userRequestDTO.getUserEmail(), String.valueOf(otpRandom));
				LOGGER.info("Hotel-Management New user password OTP :", otpRandom);
				
				user.setPassword(String.valueOf(otpRandom));
				user.setRoleType(Constants.USER_ROLE);
				user.setPhoneNumber(userRequestDTO.getPhoneNumber());
				user.setEmailId(userRequestDTO.getUserEmail());
				user.setIsDeleted(Constants.ISDELETED_FALSE);
				Timestamp currentDateAndTime = new Timestamp(System.currentTimeMillis());
				user.setCreatedDate(currentDateAndTime);
				user.setLastModifiedDate(currentDateAndTime);
				creadtedUserDetails = userService.createUser(user);
				if (creadtedUserDetails != null) {
					userResponseDTO.setUserId(creadtedUserDetails.getId());
					userResponseDTO.setUserName(creadtedUserDetails.getUserName());
					userResponseDTO.setPassword(creadtedUserDetails.getPassword());
					userResponseDTO.setRoleType(creadtedUserDetails.getRoleType());
					userResponseDTO.setMessage(Constants.USER_CREATED);
				}
				
				}else {
					userResponseDTO.setMessage(Constants.USER_ALREADY_EXISTS);
					return new ResponseEntity(userResponseDTO, HttpStatus.ALREADY_REPORTED);
				}
				
			} catch (Exception e) {
				LOGGER.info("Hotel-Management createUser Exception :", e);
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		LOGGER.info("End Hotel-Management createUser :");
		return new ResponseEntity(userResponseDTO, HttpStatus.CREATED);
	}

	@GetMapping("{id}")
	public User getUserByIds(@PathVariable long id) {
		return this.userService.getUserById(id);
	}

	@PostMapping("/login")
	public ResponseEntity<UserResponseDTO> userLogin(@RequestBody UserResponseDTO userDTO) {
		User userDetails = new User();
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		if (userDTO.getUserName() == null || userDTO.getUserName().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userDTO.getPassword() == null || userDTO.getPassword().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else {

			try {
				userDetails = this.userService.checkUserNameAndPasswordExistOrNot(userDTO.getUserName(), userDTO.getPassword());
			} catch (Exception e) {
				LOGGER.info("Hotel-Management Login Exception :", e);
			}
			if (userDetails != null) {
				userResponseDTO.setUserId(userDetails.getId());
				userResponseDTO.setUserName(userDetails.getUserName());
				userResponseDTO.setPassword(userDetails.getPassword());
				userResponseDTO.setRoleType(userDetails.getRoleType());
				userResponseDTO.setMessage(Constants.VALID_USER);
				return new ResponseEntity(userResponseDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.NOT_FOUND);
			}
		}

	}

	@PostMapping("/changePassword")
	public ResponseEntity<UserResponseDTO> userChangePassword(@RequestBody UserRequestDTO userRequestDTO) {
		Integer isUserPasswordUpdated = 0;
		User user = new User();
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		if (userRequestDTO.getExistingPassword() == null || userRequestDTO.getExistingPassword().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getNewPassword() == null || userRequestDTO.getNewPassword().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getConfirmPassword() == null || userRequestDTO.getConfirmPassword().isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else if (userRequestDTO.getExistingPassword() == userRequestDTO.getNewPassword()
				|| !userRequestDTO.getNewPassword().matches(userRequestDTO.getConfirmPassword())) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else {
			try {
				/*Map<String, String> sentOtp = getOtpmap();
				String sendOTP = sentOtp.get(userRequestDTO.getUserEmail());
				if (sendOTP.equalsIgnoreCase(userRequestDTO.getExistingPassword())) */
				user = this.userService
						.findBypassword(userRequestDTO.getExistingPassword());
				
				if(user != null)
				{
					isUserPasswordUpdated = this.userService.updatePassword(user.getUserName(),
								userRequestDTO.getNewPassword());
				} else {
					return new ResponseEntity(HttpStatus.UNAUTHORIZED);
				}
			} catch (Exception e) {
				LOGGER.info("Hotel-Management Login Exception :", e);
			}
			if (isUserPasswordUpdated == 1) {
				return new ResponseEntity(HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.NOT_FOUND);
			}
		}
	}

	@PostMapping("/forgotPassword")
	public ResponseEntity<Integer> userForgotPassword(@RequestParam String userName) {
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		int otpRandom = 0;
		Integer userDetails = 0;
		if (userName == null || userName.isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} else {
			try {
				Boolean checkUserNameExistsORNot = this.userService.authenticateByUserName(userName);

				if (checkUserNameExistsORNot) {
					otpRandom = new Random().nextInt((9999 - 100) + 1) + 10;
					addOtpmap(userName, String.valueOf(otpRandom));
					LOGGER.info("Hotel-Management forgot password OTP :", otpRandom);

					// send to mobile or emailId pending

					userDetails = this.userService.updatePassword(userName, String.valueOf(otpRandom));

				}
			} catch (Exception e) {
				LOGGER.info("Hotel-Management Login Exception :", e);
			}
			if (userDetails == 1) {
				return new ResponseEntity(userDetails,HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.NOT_FOUND);
			}
		}
	}

}
