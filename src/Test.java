import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Section;

public class Test {
	private static final String TEMPLATE_DIRECTORY_ROOT = "C:\\Users\\mahadevaswamy.tm\\Downloads\\TEMPLATE_DIRECTORY\\";

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String templateLocation = TEMPLATE_DIRECTORY_ROOT + "ExamReviewandAcceptanceForm1Template.doc";
		HWPFDocument docModified = null;

		try {
			HWPFDocument doc = new HWPFDocument(new FileInputStream(templateLocation));

			Map<String, String> map = new HashMap<>();
			map.put("[Enter Client Name Here]", "Mahesh");
			map.put("[Enter Exam name here]", "english aptitude");
			map.put("Telephone", "8105504107");
			map.put("enter price here", "499/-");
			map.put("type exam name here", "english aptitude");
			map.put("type exam language here", "english");
			map.put("type review date here", "22-01-2021");
			map.put("type reviewer�s name here", "ajay");

			for (Entry<String, String> e : map.entrySet()) {
				docModified = replaceText(doc, e.getKey(), e.getValue());

			}
			FileOutputStream fos = new FileOutputStream(TEMPLATE_DIRECTORY_ROOT + "test1.doc");
			if (docModified != null) {
				docModified.write(fos);
			}
		} catch (Exception e) {

		}
	}

	private static HWPFDocument replaceText(HWPFDocument doc, String findText, String replaceText) {
		Map<String, Range> ranges = new HashMap<>();

		ranges.put("COMMENTS", doc.getCommentsRange());
		ranges.put("ENDNOTE", doc.getEndnoteRange());
		ranges.put("FOOTNOTE", doc.getFootnoteRange());
		ranges.put("HEADERSTORY", doc.getHeaderStoryRange());
		ranges.put("MAINTEXT", doc.getMainTextboxRange());
		ranges.put("OVERALL", doc.getOverallRange());
		ranges.put("DEFAULT", doc.getRange());

		for (Entry<String, Range> e : ranges.entrySet()) {
			Range r = e.getValue();

			for (int i = 0; i < r.numSections(); ++i) {
				Section s = r.getSection(i);

				for (int j = 0; j < s.numParagraphs(); j++) {
					Paragraph p = s.getParagraph(j);

					for (int k = 0; k < p.numCharacterRuns(); k++) {
						CharacterRun run = p.getCharacterRun(k);
						String text = run.text();

						if (text.contains(findText)) {
							System.out.println("OLD:" + run.text());
							run.replaceText(findText, replaceText);
							System.out.println("NEW:" + run.text());
						}
					}
				}
			}
		}
		return doc;
	}

}
